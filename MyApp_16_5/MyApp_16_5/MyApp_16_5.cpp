﻿// MyApp_16_5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <time.h>

int main()
{
    const int N = 7;
    int myArray[N][N], i, j, summa = 0, result = 0, curDay;
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    curDay = buf.tm_mday;
    for (i=0; i < N; i++) 
    {
        for (j = 0; j < N; j++)
        {
            myArray[i][j] = i + j;
            std::cout << myArray [i][j] << " ";
            summa = summa + myArray[i][j];
        }
        std::cout << "\n";
        if (i == curDay % N)
        {
            result = summa;
        }
        summa = 0;
    }
    std::cout << "Your result: " << result;
}

